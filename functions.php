<?php
function wp_lb_enqueue_scripts()
{
    wp_enqueue_script('polyfill-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/polyfill.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script('tilt-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/tilt.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script('slick-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/slick.min.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script('lity-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/lity.min.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script('moment-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/moment.min.js', array( 'jquery' ), '1.0', true);
    wp_enqueue_script('lb_custom-js', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/lb_custom.js', array( 'jquery' ), '1.0', true);
    
    $styleWP = get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/style.css';
	$stylePath = __DIR__ . '/style.css';
	$filetime = filemtime($stylePath);
 
    wp_enqueue_style( 'child_style', $styleWP, array(), $filetime );
    
    if ( is_page( [23, 'donate'] ) ) {
        wp_enqueue_script( 'formatcurrency', get_stylesheet_directory_uri() . '../../../plugins/beard-oil/assets/js/formatcurrency.js', array( 'jquery' ), '1.0', true );
    }
};
add_action('wp_enqueue_scripts', 'wp_lb_enqueue_scripts', 11);
 


if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// Enqueue Google Analytics to header
function init_analytics()
{
    $analyticsUA = 'UA-153751939-1';
    $analytics = '<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=' . $analyticsUA . '"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag(\'js\', new Date());

  gtag(\'config\', \'' . $analyticsUA . '\');
</script>';
    
    echo "\n" . $analytics;
}

// add_action('wp_head', 'init_analytics', 35);

function init_pixel()
{
    $pixelID = '440845819908098';
    $pixel = '<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,\'script\',
  \'https://connect.facebook.net/en_US/fbevents.js\');
  fbq(\'init\', \'' . $pixelID . '\');
  fbq(\'track\', \'PageView\');
</script>
<noscript><img height=\'1\' width=\'1\' style=\'display:none\'
  src=\'https://www.facebook.com/tr?id=' . $pixelID . ' &ev=PageView&noscript=1\'
/></noscript><!-- End Facebook Pixel Code -->';

    echo "\n" . $pixel;
}

add_action('wp_head', 'init_pixel', 35);

function init_favicons()
{
    $favicons = '<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/site.webmanifest">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">';

    echo "\n" . $favicons;
}

add_action('wp_head', 'init_favicons', 10);

// Enqueue Google Analytics to header
function init_ytverification()
{
    $yt = '<meta name="google-site-verification" content="Vk0n3nj7-20OXxdm05Ff1DdzBc1CE8sByIKQQ-4_rZo" />';
    
    echo "\n" . $yt;
}

add_action('wp_head', 'init_ytverification', 35);

function cookie_consent()
{
    // Add policies here
    $privacyPolicyURL = '/privacy-policy/';
    $cookiePolicyURL = '/cookie-policy/';

    $cookiesEN = '<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
  <script>
  window.addEventListener("load", function(){
  window.cookieconsent.initialise({
    "palette": {
      "popup": {
        "background": "#F1EDEA",
        "text": "#636267"
      },
      "button": {
        "background": "#9A6D32",
        "text": "#ffffff",
      }
    },
    "showLink": false,
    "content": {
      "message": "By using this website, you agree to our <a href=\"' . $privacyPolicyURL . '\" target=\"_blank\">Privacy Policy</a> and <a href=\"' . $cookiePolicyURL . '\" target=\"_blank\">Terms of Use.</a>",
            "dismiss": "Accept"
    }
  })});
  </script>';
  
    echo "\n" . $cookiesEN;
}

add_action('wp_head', 'cookie_consent', 10);


function wpdocs_my_search_form($form)
{
    $form = '<form role="search" method="get" id="searchform" class="searchform" action="' . home_url('/') . '" >
  <div><label class="screen-reader-text" for="s">' . __('Search for:') . '</label>
  <input type="text" value="" placeholder="Search..." name="s" id="s" />
  <button class="button" type="submit" id="searchsubmit" value="Search">Search</button>
  </div>
  </form>';

    return $form;
}
add_filter('get_search_form', 'wpdocs_my_search_form');

// filter the Gravity Forms button type
add_filter('gform_submit_button_1', 'contact_button', 10, 2);
function contact_button($button, $form)
{
    return "<button class='button submit-button gform_button' id='gform_submit_button_{$form['id']}'>Send</button>";
}

// filter the Gravity Forms button type
add_filter('gform_submit_button_2', 'donate_button', 10, 2);
function donate_button($button, $form)
{
    return "<button class='button submit-button gform_button' id='gform_submit_button_{$form['id']}'>Donate</button>";
}


// filter the Gravity Forms button type
add_filter('gform_submit_button_3', 'newsletter_button', 10, 2);
function newsletter_button($button, $form)
{
    return "<button class='button submit-button gform_button' id='gform_submit_button_{$form['id']}'>Sign up</button>";
}
// Woocommerce

function woo_rename_tabs($tabs)
{
    $tabs['description']['title'] = __('EXTRA INFO');		// Rename the description tab
    $tabs['additional_information']['title'] = __('Pricing and Shipping INFO');	// Rename the additional information tab

    return $tabs;
}
add_filter('woocommerce_product_tabs', 'woo_rename_tabs', 98);




add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs)
{
    unset($tabs['description']);      	// Remove the description tab
    unset($tabs['additional_information']);  	// Remove the additional information tab

    return $tabs;
}


// add two new tabs
add_filter('woocommerce_product_tabs', 'woo_new_extra_info_tab');
add_filter('woocommerce_product_tabs', 'woo_new_pricing_shipping_tab');

function woo_new_extra_info_tab($tabs)
{
    if (get_field('extra_info', get_the_ID())) {
        $tabs['extra-info'] = array(
        'title' 	=> __('EXTRA INFO', 'woocommerce'),
        'priority' 	=> 1,
        'callback' 	=> 'woo_new_extra_info_tab_content'
    );
        return $tabs;
    }
}

function woo_new_extra_info_tab_content()
{
    echo get_field('extra_info', get_the_ID());
}


function woo_new_pricing_shipping_tab($tabs)
{
    if (get_field('pricing_and_shipping_info', get_the_ID())) {
        $tabs['pricing-shipping'] = array(
        'title' 	=> __('Pricing and Shipping INFO', 'woocommerce'),
        'priority' 	=> 2,
        'callback' 	=> 'woo_new_pricing_shipping_tab_content'
    );
        return $tabs;
    }
}

function woo_new_pricing_shipping_tab_content()
{
    echo get_field('pricing_and_shipping_info', get_the_ID());
}

// Remove Empty Tabs
// add_filter('woocommerce_product_tabs', 'yikes_woo_remove_empty_tabs', 20, 1);

function yikes_woo_remove_empty_tabs($tabs)
{
    if (! empty($tabs)) {
        foreach ($tabs as $title => $tab) {
            if (empty($tab['content']) && strtolower($tab['title']) !== 'description') {
                unset($tabs[ $title ]);
            }
        }
    }
    return $tabs;
}


// for items in the loop
add_action('woocommerce_before_shop_loop_item_title', 'shop_product_item_before', 9);

function shop_product_item_before()
{
    $taxonomy     = 'product_cat';
    $orderby      = 'name';
    $show_count   = 0;      // 1 for yes, 0 for no
    $pad_counts   = 0;      // 1 for yes, 0 for no
    $hierarchical = 1;      // 1 for yes, 0 for no
    $title        = '';
    $empty        = 0;

    $args = array(
            'object_ids'   => array(get_the_ID()),
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty
    );
    $all_categories = get_categories($args);
    echo '<div class="cat-wrapper">';
    foreach ($all_categories as $cat) {
        if ($cat->category_parent == 0) {
            $category_id = $cat->term_id;
            echo '<span class="cat">'. $cat->name .'</span>';
        }
    }
    echo '</div>';
    if (get_field('subtitle', get_the_ID())) :
        echo '<h6 class="subtitle">'.get_field('subtitle', get_the_ID()).'</h6>';
    endif;
}


function custom_related_products_text($translated_text, $text, $domain)
{
    switch ($translated_text) {
      case 'Related products':
        $translated_text = __('Similar Products', 'woocommerce');
        break;
    }
    return $translated_text;
}
add_filter('gettext', 'custom_related_products_text', 20, 3);

function remove_image_zoom_support()
{
    remove_theme_support('wc-product-gallery-zoom');
}
// add_action('wp', 'remove_image_zoom_support', 100);

/*
* Remove Ajax from The Events Calendar Pagination on Month, List, and Day Views
* @4.0.6
*
*/
function events_calendar_remove_scripts() {
if (!is_admin() && !in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {

        wp_dequeue_script( 'the-events-calendar');
       // wp_dequeue_script( 'tribe-events-list');
        wp_dequeue_script( 'tribe-events-ajax-day');

}}
add_action('wp_print_scripts', 'events_calendar_remove_scripts' , 10);
add_action('wp_footer', 'events_calendar_remove_scripts' , 10);



add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );


// ADD FREE DVD NOTICE TO CART PAGE

add_action( 'woocommerce_before_cart', 'wnd_checkout_code', 10 );

function wnd_checkout_code( ) {

    // 1. Set vars for conditions to exclude the notice
    $in_cart = false;
    $product_ids = [37, 462]; // Live to Mass (dev, live)

    // 2. Check if Product is in cart
    foreach( WC()->cart->get_cart() as $cart_item ) {
        $product_in_cart = $cart_item['product_id'];
        if ( in_array( $product_in_cart, $product_ids ) ) {
            $in_cart = true;
            break;
        }
    }

    // 3. Display notice only if product is not in cart
    if ( ! $in_cart ) {
        $notice = 'If you don’t have access to YouTube or <a href="http://theveilremoved.com/" target="_blank">our website</a> to view the short film, you may request a DVD along with a valid purchase. <a href="https://theveilremoved.com/shop/the-veil-removed-dvd/" target="_blank">Click here to add the DVD to your order</a>.';
        wc_print_notice( $notice, 'notice' );
    }
    
}