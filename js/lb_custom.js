var $ = jQuery.noConflict();

/*********************
ANONYMOUS FUNCTIONS
*********************/

$(document).ready(function () {
  console.log("JMJ");
  sliderTilt(".what-people-are-saying-nav .person-pic");
  witnessesPopup();
  lb_tabs(".tab-nav .tab-nav-inner .tab-nav-item");
  lb_search_keys(".search-area input", ".search-wrapper", ".accordion-sml");
  accordionsInit();
  mobileMenu(".mobile-btn", ".mobile-menu");
  lbDonate("#input_2_5 > li > input", "#input_2_19", ".ginput_total_2");
  quoteSlider(".slider-text-container", ".slider-image-container");
  imageSlider(".slider-image-container", ".slider-text-container");
  videoSlider("#code_block-8-34");
  youtubeInfoAPI(".page-id-34 .video");
  fixTileAlignment(".tiles");
  homepageSlider();
  homepagePopup();
  lb_smooth_scroll();
  didYouKnowPopups(".point .button");
  prayerPopup(".card-link[href='#prayer']");
  popUpShareButtons();
  newReadMore(".founder-read-more", [
    {
      numberToShow: 3,
      viewportSize: 9999999,
    },
    {
      numberToShow: 2,
      viewportSize: 1024,
    },
  ]);
  newReadMore(".our-inspiration-read-more", [
    {
      numberToShow: 1,
      viewportSize: 600,
    },
  ]);
  newReadMore(".home-read-more", [
    {
      numberToShow: 3,
      viewportSize: 9999999,
    },
  ]);
  productTabs(".wc-tabs-wrapper .wc-tabs li a", ".wc-tabs-wrapper .wc-tab");
  productLookInsideImages(".flex-viewport");
  productActiveThumbnails(".flex-control-thumbs li img");
  checkoutAlert();
});

window.almComplete = function (alm) {
  newsPageFeaturedArea(alm);
};

/*********************
DECLARED FUNCTIONS
*********************/
function newsPageFeaturedArea(alm) {
  if (alm.page === 0 && alm.orginal_posts_per_page === 3) {
    var childrenToWrap = $(alm.el).children().slice(1);
    $(childrenToWrap).wrapAll(
      "<div class='featured-side-flex'><div class='featured-side-wrapper'><div class='featured-side-slider'></div></div></div>"
    );
    return;
  }
  return;
}

function productActiveThumbnails(ele) {
  $(ele).each(function () {
    if ($(this).hasClass("flex-active")) {
      $(this).parent().addClass("active");
    }
  });
  $(ele).click(function () {
    $(ele).each(function () {
      $(this).parent().removeClass("active");
    });
    $(this).parent().addClass("active");
  });
}
function productLookInsideImages(ele) {
  $(ele).append(
    '<div class="woocommerce-product-gallery__trigger"><span class="button pop-up">Look Inside</span></div>'
  );
}
function productTabs(eleTabNav, eleTabBodies) {
  $(eleTabNav).click(function (e) {
    e.preventDefault();
    var targetBody = $(this).attr("href");

    $(eleTabBodies).each(function () {
      $(this).removeClass("active");
    });
    $(eleTabBodies + targetBody).addClass("active");
  });
}
function homepageSlider() {
  var items = ".what-people-are-saying-nav .person-pic";
  var quoteSliderEle = ".what-people-are-saying-slider";
  var imageSliderEle = ".what-people-are-saying-pics";

  quoteSlider(quoteSliderEle, imageSliderEle);
  imageSlider(imageSliderEle, quoteSliderEle);

  $(items).click(function () {
    $(items).each(function () {
      $(this).removeClass("active");
    });
    $(this).addClass("active");
    $(quoteSliderEle).slick("slickGoTo", $(this).index());
    $(imageSliderEle).slick("slickGoTo", $(this).index());
  });
  $(items).first().click();

  $(quoteSliderEle).on("beforeChange", function (
    event,
    slick,
    currentSlide,
    nextSlide
  ) {
    $(items).each(function () {
      $(this).removeClass("active");
    });
    $(items).eq(nextSlide).addClass("active");
  });
}

function homepagePopup() {
  var popup = $('.hero-popup');
  var popupShown = false;
  if (!popup.length) return;

  $(window).on('load resize scroll', function () {
    if (isScrolledIntoView(popup[0]) && !popupShown) {
      popup.animate({
        opacity: 1,
      }, 250, function () {
        popupShown = true;
      });
    }
  })

  var close = popup.find('.close');
  close.on('click', function () {
    popup.stop().animate({
      opacity: 0,
      right: "-=10",
    }, 250, function () {
      popup.hide();
    });
  })

  function isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;

    // Only completely visible elements return true:
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
  }
}

function didYouKnowPopups(ele) {
  $(ele).click(function (e) {
    var target = $(this);
    e.preventDefault();
    function hydratePopUp(ele) {
      var didYouKnowHTML =
        '<div class="didYouKnow-inner">' + $(ele).attr("data-info") + "</div>";
      $(".lb-popup .lb-popup-container-inner").append(didYouKnowHTML);
    }
    popUpConstructor();
    hydratePopUp(target);
    setTimeout(function () {
      $(".lb-popup").addClass("active");
    }, 500);
  });
}

function prayerPopup(ele) {
  $(ele).click(function (e) {
    var target = $(this);
    e.preventDefault();
    function hydratePopUp(ele) {
      var didYouKnowHTML =
        '<div class="prayer-mary-inner"><div class="img-col"><img src="' +
        $(ele).attr("data-image") +
        '" alt="Blessed Virgin Mary" /><div class="text">' +
        $(ele).attr("data-title-quote") +
        '</div></div><div class="info-col">' +
        $(ele).attr("data-title-quote") +
        $(ele).attr("data-content") +
        "</div></div>";
      $(".lb-popup .lb-popup-container-inner").append(didYouKnowHTML);
    }
    popUpConstructor("prayer-mary");
    hydratePopUp(target);
    setTimeout(function () {
      $(".lb-popup").addClass("active");
    }, 500);
  });
}

function fixTileAlignment(ele) {
  var topRow = $(ele).find(".top-row");
  var bottomRow = $(ele).find(".bottom-row");

  function getLastChild(ele) {
    return $(ele).children().last();
  }

  var widthDifference =
    getLastChild(topRow).width() - getLastChild(bottomRow).width();
  getLastChild(bottomRow).css("margin-right", widthDifference + "px");
}

function mobileMenu(btn, menu) {
  $(btn).click(function () {
    $("header").toggleClass("active");
    $(this).toggleClass("active");
    $(menu).toggleClass("active");

    $("body").toggleClass("menu-active");

    if (!$(menu).hasClass("active")) {
      $("#menu-main-1 li.menu-item-has-children").each(function () {
        $(this).removeClass("active").siblings(".lb-subnav").slideUp();
        $(this).parent().removeClass("active");
      });
    }
  });

  $(menu + " li.menu-item-has-children").click(function (e) {
    $(this).toggleClass("active").children(".sub-menu").slideToggle();
    $(this).parent().toggleClass("active");
    e.stopPropagation();
  });
}

function sliderTilt(ele) {
  var tilted = $(ele).tilt({
    maxTilt: 1,
  });

  tilted.on("change", function (e, transforms) {
    $(this).css(
      "transform",
      "translate(" +
      transforms.tiltX * -50 +
      "%," +
      transforms.tiltY * 50 +
      "%) scale(1)"
    );
  });
}
function witnessesPopup() {
  if ($("body").hasClass("page-id-20")) {
    $(".saint .pop-up.button").click(function (e) {
      e.preventDefault();
      var button = $(this);

      function createSaint(ele) {
        var creatSaint = {
          title: $(ele).closest(".info").find(".title").text(),
          excerpt: $(ele).closest(".info").find(".excerpt").text(),
          description: $(ele).attr("data-description"),
          linkText: $(ele).attr("data-outside-link-text"),
          linkLink: $(ele).attr("data-outside-link-link"),
        };
        return creatSaint;
      }

      function hydratePopUp(ele) {
        var saint = createSaint(ele);
        // check to see if data exists
        function outsideButton() {
          if (saint.linkLink !== undefined && saint.linkText !== undefined) {
            return (
              '<a href="' +
              saint.linkLink +
              '" class="button outside-link" target="_blank">' +
              saint.linkText +
              "</a>"
            );
          }
          return "";
        }

        function excerpt() {
          if (saint.excerpt !== undefined) {
            return '<h6 class="excerpt">' + saint.excerpt + "</h6>";
          }
          return "";
        }

        function description() {
          if (saint.description !== undefined) {
            return '<div class="desc-inner">' + saint.description + "</div>";
          }
          return "";
        }

        var saintHTML =
          '<h2 class="title-mobile">' +
          saint.title +
          '</h2><div class="large-10"><div class="info"><h2 class="title">' +
          saint.title +
          "</h2>" +
          excerpt() +
          outsideButton() +
          '</div><div class="desc">' +
          description() +
          "</div></div>";
        $(".lb-popup .lb-popup-container-inner").append(saintHTML);
      }

      popUpConstructor();
      hydratePopUp(button);
      setTimeout(function () {
        $(".lb-popup").addClass("active");
      }, 500);
    });
    return;
  }
}
function lb_tabs(ele) {
  var tabs = $('.tabs');
  tabs.each(function () {
    var $this = $(this);
    var lbButton = $(this).find('.tab-nav .tab-nav-inner .tab-nav-item');
    $(lbButton).click(function (e) {
      if (!$(this).hasClass("non-tab")) {
        e.preventDefault();
      }
      var clicked = $(this);
      var lbHref = $(this).attr("href");
      $(lbButton).each(function () {
        // exclude from subtabs / parent tabs
        if (clicked.closest('.tabs .tabs').length !== $(this).closest('.tabs .tabs').length) return true;
        $(this).removeClass("active");
      });
      $(this).addClass("active");
      $(this)
        .closest(".tabs")
        .find(".tab-body")
        .each(function () {
          // exclude from subtabs / parent tabs
          if (clicked.closest('.tabs .tabs').length !== $(this).closest('.tabs .tabs').length) return true;
          $(this).removeClass("active");
        });
      $(".tab-body" + lbHref).addClass("active");
    });
    $(lbButton).first().click();

    $(window).on('load resize', function () {
      var inner = $this.find('.tab-nav-inner');
      if (inner.outerWidth() < inner.get(0).scrollWidth) {
        $this.addClass('overflow');
      } else {
        $this.removeClass('overflow')
      }
    })


    // $this.css({ 'opacity': 0 });
    // $(lbButton).first().click();
    // $(window).load(function () {
    //   var hash = location.hash;
    //   if (!hash) {
    //     $this.animate({ 'opacity': 1 }, 250);
    //   } else {
    //     setTimeout(function () {
    //       if ($(lbButton).filter('[href="' + hash + '"]').length) {
    //         $(lbButton).filter('[href="' + hash + '"]').click();
    //         if ($(lbButton).filter('[href="' + hash + '"]').closest('.tabs .tabs').length) {
    //           var parentId = $(lbButton).filter('[href="' + hash + '"]').closest('.tabs .tab-body').attr('id');
    //           $(lbButton).filter('[href="#' + parentId + '"]').click();
    //         }
    //       } else {
    //         $(lbButton).first().click();
    //       }
    //       $this.animate({ 'opacity': 1 }, 250);
    //     }, 50);
    //   }
    // })
  })
}

function accessibility() {
  // Remove focus from links
  $("body").addClass("no-focus-outline");

  // Listen to tab events to enable outlines (accessibility improvement)
  document.body.addEventListener("keyup", function (e) {
    if (e.which === 9) {
      document.body.classList.remove("no-focus-outline");
    }
  });

  // Helper function for Radio buttons and Check boxes.
  // Right now we need this since we actually remove input element so we can replace it with a stylized version
  function RandCB(ele) {
    $(ele).attr("tabindex", "0");
    $(ele).on("keypress", function (e) {
      if (e.which == 13) {
        $(this).click();
      }
    });
  }

  var linkTitles = ["facebook", "instagram", "twitter", "rss", "youtube"];

  for (i = 0; i < linkTitles.length; ++i) {
    $(".oxy-social-icons")
      .find("a")
      .each(function () {
        if ($(this).attr("class").indexOf(linkTitles[i]) >= 0) {
          $(this).attr("title", linkTitles[i] + " link");
          $(this).attr("aria-label", linkTitles[i] + " link");
        }
      });
  }
}

function mobileSearch() {
  if ($(window).width() <= 600) {
    $(".search-bar svg").click(function () {
      $(this).parents().find(".search-input").toggleClass("active");
    });
  }
}

function slickSlider(slider) {
  $(window).on("load resize orientationchange", function () {
    $(slider).each(function () {
      var $carousel = $(this);
      /* Initializes a slick carousel only on mobile screens */

      // slick on mobile
      if ($(window).width() > 1025) {
        if ($carousel.hasClass("slick-initialized")) {
          $carousel.slick("unslick");
        }
      } else {
        if (!$carousel.hasClass("slick-initialized")) {
          $carousel.slick({
            infinite: true,
            speed: 400,
            autoplay: false,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            prevArrow: '<div class="slick-arrow slick-prev">Prev</div>', //replace arrows
            nextArrow: '<div class="slick-arrow slick-next">Next></div>', //replace arrows
            responsive: [
              {
                breakpoint: 601,
                settings: "unslick",
              },
            ],
          });
        }
      }
    });
  });
}

function lb_smooth_scroll() {
  if (window.location.hash && $('[id="' + window.location.hash + '"]').length) {
    $("html, body").animate(
      {
        scrollTop: $(window.location.hash).offset().top - 150,
      },
      1000
    );
  }

  // Select all links with hashes
  $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .not('[href*="#_"]')
    .not(".tab-nav-item")
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          $("html, body").animate(
            {
              scrollTop: target.offset().top - 150,
            },
            1000,
            function () {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                // Checking if the target was focused
                return false;
              } else {
                $target.attr("tabindex", "-1"); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              }
            }
          );
        }
      }
    });
}

function lbDonate(radioFields, userFields, totalField) {
  $('.gfield_radio input[value="gf_other_choice"]').each(function () {
    // $(this).after('<label><span class="screen-reader-text">Other</span></label>');

    var radio = $(this);
    var text = $(this).siblings('input[type="text"]');
    var fieldId = radio.attr('id');

    radio.before(
      '<label for="' + fieldId + '" id="' + fieldId.replace('choice', 'label') + '"></label>'
    );

    var label = $(this).siblings('label');

    label.click(function () {
      $(this).addClass('checked');
      radio.trigger('click');
      text.trigger('focus');
    })

    text
      .focus(function () {
        label.addClass('checked');
        radio.trigger('click');
      })
      .blur(function () {
        if ($(this).val() <= 0 || !$.isNumeric($(this).val())) {
          $(this).val('');
          radio.prop('checked', false);
        } else {
          $(this).formatCurrency();
          radio.trigger('click');
          radio.blur();
        }
      })
      .on('input', function () {
        var val = $(this).val();
        radio.val(val);
      })

    $(this).parent().siblings().click(function () {
      label.removeClass('checked');
    })
  })

  // // remove the $ from the value we get
  // function getRawVal(str) {
  //   return str.substring(1, str.length);
  // }

  // // update the user and total field
  // function updateFormValues(val) {
  //   // update the field the user sees
  //   if ($.isArray(userFields)) {
  //     userFields.map(function (userField) {
  //       $(userField).attr("value", val).change();
  //     });
  //   } else {
  //     $(userFields).attr("value", val).change();
  //   }
  //   // update the total field so it can be processed correctly
  //   $(totalField).html("$" + val + ".00");
  // }

  // // update on load
  // $(radioFields).each(function () {
  //   if ($(this).is(":checked")) {
  //     updateFormValues(getRawVal($(this).val()));
  //   }
  // });

  // // update on change
  // $(radioFields).change(function () {
  //   updateFormValues(getRawVal($(this).val()));
  // });
}
function quoteSlider(ele, asNavFor) {
  if (asNavFor === undefined) {
    asNavFor = null;
  }
  $(ele).slick({
    infinite: true,
    speed: 400,
    autoplay: true,
    autoplaySpeed: 10000,
    dots: true,
    arrows: false,
    asNavFor: asNavFor,
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });
}

function imageSlider(ele, asNavFor) {
  if (asNavFor === undefined) {
    asNavFor = null;
  }
  $(ele).slick({
    infinite: true,
    speed: 400,
    autoplay: true,
    autoplaySpeed: 10000,
    dots: false,
    arrows: false,
    asNavFor: asNavFor,
    centerMode: false,
    slidesToShow: 1,
    slidesToScroll: 1,
  });
}

function videoSlider(ele) {
  if ($(window).width() <= 600) {
    $(ele).slick({
      infinite: true,
      speed: 400,
      autoplay: true,
      autoplaySpeed: 10000,
      dots: true,
      arrows: false,
      centerMode: false,
      slidesToShow: 1,
      slidesToScroll: 1,
    });
  }
}

/*********************
HELPER METHODS
*********************/
function accordionsInit() {
  function accordionFunc(ele) {
    $(ele + " " + ele + "-inner > .title").click(function (e) {
      $(this).toggleClass("active");
      $(this).siblings().slideToggle();

      e.stopPropagation();
    });
  }

  accordionFunc(".accordion-sml");
}

function popUpConstructor(bodyClass) {
  var popupBodyClass = bodyClass ? bodyClass : "";
  function closePopup(ele) {
    $(ele).click(function (e) {
      e.stopPropagation();
      $(".lb-popup").removeClass("ready");
      setTimeout(function () {
        $(".lb-popup").remove();
        $("body").removeClass("popup-active");
      }, 260);
    });
  }

  $("body").append(
    '<div class="lb-popup ' +
    popupBodyClass +
    '"><div class="lb-popup-inner"><div class="lb-popup-container grid-container"><div class="close-popup"></div><div class="lb-popup-container-inner grid-x"></div></div></div></div>'
  );
  $("body").addClass("popup-active");
  setTimeout(function () {
    $(".lb-popup").addClass("ready");
  }, 10);

  document
    .querySelector(".lb-popup .lb-popup-container")
    .addEventListener("click", function (e) {
      e.stopPropagation();
    });

  $(".lb-popup").click(function (e) {
    if ($("body").hasClass("popup-active")) {
      e.stopPropagation();
      $(".lb-popup").removeClass("ready");
      setTimeout(function () {
        $(".lb-popup").remove();
        $("body").removeClass("popup-active");
      }, 260);
    }
  });

  closePopup(".close-popup");
}

function lb_search_keys(input, itemsWrapper, eleSearched) {
  function handleKeyUp() {
    var value = $(this).val().toLowerCase();

    var searchedItems = $(this)
      .closest(itemsWrapper)
      .find(eleSearched)
      .filter(function () {
        $(this)
          .closest(itemsWrapper + " " + eleSearched)
          .toggle($(this).text().toLowerCase().indexOf(value) > -1);
        return (
          $(this)
            .closest(itemsWrapper + " " + eleSearched)
            .text()
            .toLowerCase()
            .indexOf(value) > -1
        );
      });

    if (searchedItems.length <= 0 && $(".no-items-found").length <= 0) {
      $(this)
        .closest(itemsWrapper)
        .append('<h3 class="no-items-found" >No items found</h3>');
    }

    if (searchedItems.length > 0) {
      $(this).closest(itemsWrapper).find(".no-items-found").remove();
    }

    $(this)
      .closest(itemsWrapper)
      .find(".results .total-viewing")
      .html(searchedItems.length);
  }
  $(input).on("keyup", handleKeyUp);
  $(input).each(function () {
    handleKeyUp.call($(this));
  });
}

function popUpShareButtons() {
  $(document).on("lity:ready", function (event, instance) {
    console.log(instance.opener());
    var openerEle = instance.opener();
    var linkToShare = openerEle.context.href;
    var lityContainer = $(instance.element()).find(".lity-container");

    function hydrateShareButtons(url) {
      var facebookSVG =
        '<svg id="FontAwesomeicon-facebook" viewBox="0 0 19 32"><title>facebook</title><path class="path1" d="M17.125 0.214v4.714h-2.804q-1.536 0-2.071 0.643t-0.536 1.929v3.375h5.232l-0.696 5.286h-4.536v13.554h-5.464v-13.554h-4.554v-5.286h4.554v-3.893q0-3.321 1.857-5.152t4.946-1.83q2.625 0 4.071 0.214z"></path></svg>';
      var twitter =
        '<svg id="FontAwesomeicon-twitter" viewBox="0 0 30 32"><title>twitter</title><path class="path1" d="M28.929 7.286q-1.196 1.75-2.893 2.982 0.018 0.25 0.018 0.75 0 2.321-0.679 4.634t-2.063 4.437-3.295 3.759-4.607 2.607-5.768 0.973q-4.839 0-8.857-2.589 0.625 0.071 1.393 0.071 4.018 0 7.161-2.464-1.875-0.036-3.357-1.152t-2.036-2.848q0.589 0.089 1.089 0.089 0.768 0 1.518-0.196-2-0.411-3.313-1.991t-1.313-3.67v-0.071q1.214 0.679 2.607 0.732-1.179-0.786-1.875-2.054t-0.696-2.75q0-1.571 0.786-2.911 2.161 2.661 5.259 4.259t6.634 1.777q-0.143-0.679-0.143-1.321 0-2.393 1.688-4.080t4.080-1.688q2.5 0 4.214 1.821 1.946-0.375 3.661-1.393-0.661 2.054-2.536 3.179 1.661-0.179 3.321-0.893z"></path></svg>';
      var link =
        '<svg xmlns="http://www.w3.org/2000/svg" width="14.998" height="15" viewBox="0 0 14.998 15"><g id="link" transform="translate(-0.039 0)"><g id="Group_3001" data-name="Group 3001" transform="translate(0.039 4.822)"><g id="Group_3000" data-name="Group 3000" transform="translate(0)"><path id="Path_21988" data-name="Path 21988" d="M9.19,165.639a3.4,3.4,0,0,0-.587-.472,3.5,3.5,0,0,0-4.362.472l-3.179,3.182a3.5,3.5,0,0,0,4.947,4.952l2.625-2.625a.25.25,0,0,0-.177-.427h-.1a4.207,4.207,0,0,1-1.6-.31.25.25,0,0,0-.272.055L4.6,172.355a1.5,1.5,0,0,1-2.122-2.122l3.192-3.189a1.5,1.5,0,0,1,2.12,0,1.025,1.025,0,0,0,1.4,0,.99.99,0,0,0,0-1.4Z" transform="translate(-0.039 -164.614)" fill="#161d38"/></g></g><g id="Group_3003" data-name="Group 3003" transform="translate(5.589 0)"><g id="Group_3002" data-name="Group 3002" transform="translate(0 0)"><path id="Path_21989" data-name="Path 21989" d="M197.925,1.025a3.5,3.5,0,0,0-4.949,0l-2.622,2.62a.251.251,0,0,0,.182.427h.092a4.2,4.2,0,0,1,1.6.312.25.25,0,0,0,.272-.055l1.882-1.88A1.5,1.5,0,1,1,196.5,4.572l-2.345,2.342-.02.022-.82.815a1.5,1.5,0,0,1-2.12,0,1.025,1.025,0,0,0-1.4,0,1,1,0,0,0,0,1.41,3.469,3.469,0,0,0,1,.7c.052.025.1.045.157.067s.107.04.16.06.107.037.16.052l.147.04c.1.025.2.045.3.062a3.479,3.479,0,0,0,.372.035h.19l.15-.017c.055,0,.112-.015.177-.015h.085l.172-.025.08-.015.145-.03h.028a3.5,3.5,0,0,0,1.622-.92l3.182-3.182A3.5,3.5,0,0,0,197.925,1.025Z" transform="translate(-189.502 0)" fill="#161d38"/></g></g></g></svg>';

      var html =
        '<div class="share-menu"><a href="https://www.facebook.com/sharer/sharer.php?u=' +
        url +
        '" target="_blank">' +
        facebookSVG +
        '</a><a href="https://twitter.com/share?text=' +
        url +
        '" target="_blank">' +
        twitter +
        '</a><a href="#" data-link="' +
        url +
        '">' +
        link +
        "</a></div>";
      $(lityContainer).append(html);
      setTimeout($(".share-menu").addClass("active"), 1000);
    }

    hydrateShareButtons(linkToShare);

    if (openerEle.context.innerText == "Play The Film") {
      var section = "#section-28-12";
      var learnMoreHtml =
        '<div class="learn-more-scroll"><a class="scrolly" href="#">Learn More<svg xmlns="http://www.w3.org/2000/svg" width="15.96" height="24.132" viewBox="0 0 15.96 24.132"> <g id="Group_3647" data-name="Group 3647" transform="translate(-988.086 -831.5)"><line id="Line_273" data-name="Line 273" y2="20.708" transform="translate(996.066 832.5)" fill="none" stroke="#fade2f" stroke-linecap="round" stroke-width="2"/><line id="Line_274" data-name="Line 274" x2="6.566" y2="6.566" transform="translate(989.5 847.652)" fill="none" stroke="#fade2f" stroke-linecap="round" stroke-width="2"/><line id="Line_275" data-name="Line 275" y1="6.566" x2="6.566" transform="translate(996.066 847.652)" fill="none" stroke="#fade2f" stroke-linecap="round" stroke-width="2"/></g></svg></a><a href="/shop/">Shop<svg id="shop" xmlns="http://www.w3.org/2000/svg" width="22.323" height="22.541" viewBox="0 0 22.323 22.541"><path id="Path_22175" data-name="Path 22175" d="M147.03,401.668a2.429,2.429,0,1,0,2.429,2.429A2.431,2.431,0,0,0,147.03,401.668Zm0,0" transform="translate(-138.236 -383.985)" fill="#fade2f"/><path id="Path_22176" data-name="Path 22176" d="M323.761,401.668a2.429,2.429,0,1,0,2.429,2.429A2.432,2.432,0,0,0,323.761,401.668Zm0,0" transform="translate(-307.185 -383.985)" fill="#fade2f"/><path id="Path_22177" data-name="Path 22177" d="M83.225,81.072a2.191,2.191,0,0,0-1.677-.74H64.926a.66.66,0,1,0,0,1.321h.088l1.239,9.3a.66.66,0,0,0,.087,1.314H80.134a2.741,2.741,0,0,0,2.657-2.326l.947-7.1A2.191,2.191,0,0,0,83.225,81.072Zm0,0" transform="translate(-61.436 -76.795)" fill="#f5eaa3"/><path id="Path_22179" data-name="Path 22179" d="M97.968,85.638H92.9l.307-4.6a.66.66,0,0,0-1.318-.088l-.312,4.688H87.859l-.313-4.688a.66.66,0,0,0-1.318.088l.307,4.6H80.992a.66.66,0,0,0,0,1.321h5.631l.313,4.689a.661.661,0,0,0,.658.617l.045,0a.66.66,0,0,0,.615-.7l-.307-4.6h3.538l-.307,4.6a.66.66,0,0,0,.615.7l.045,0a.66.66,0,0,0,.658-.617l.313-4.689h5.159a.66.66,0,1,0,0-1.321Zm0,0" transform="translate(-76.795 -76.796)" fill="#f7f7f7"/><path id="Path_22180" data-name="Path 22180" d="M301.737,85.64h-5.071l.307-4.6a.66.66,0,1,0-1.318-.088l-.312,4.688h-1.857v1.321h1.769l-.307,4.6a.66.66,0,0,0,.615.7l.045,0a.66.66,0,0,0,.658-.617l.313-4.689h5.159a.66.66,0,0,0,0-1.321Zm0,0" transform="translate(-280.564 -76.799)" fill="#f7f7f7"/><path id="Path_22181" data-name="Path 22181" d="M20.466,19H7.144a2.742,2.742,0,0,1-2.657-2.326L2.6,2.5c-.1-.732-.546-1.18-.758-1.18H.66A.66.66,0,1,1,.66,0H1.839A2.449,2.449,0,0,1,3.907,2.326L5.8,16.5a1.417,1.417,0,0,0,1.347,1.18H20.466a.66.66,0,0,1,0,1.321Zm0,0" transform="translate(0 0)" fill="#fade2f"/></svg></a></div>';
      $(lityContainer).append(learnMoreHtml);

      $(".learn-more-scroll .scrolly").click(function () {
        instance.close().then(function () {
          $("html, body").animate(
            {
              scrollTop: $(section).offset().top - 325,
            },
            1500
          );
        });
      });
    }
  });
}

function slugify(string) {
  var a =
    "àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
  var b =
    "aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
  var p = new RegExp(a.split("").join("|"), "g");
  return string
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-") // Replace spaces with -
    .replace(p, function (c) {
      return b.charAt(a.indexOf(c));
    }) // Replace special characters
    .replace(/&/g, "-and-") // Replace & with 'and'
    .replace(/[^\w\-]+/g, "") // Remove all non-word characters
    .replace(/\-\-+/g, "-") // Replace multiple - with single -
    .replace(/^-+/, "") // Trim - from start of text
    .replace(/-+$/, ""); // Trim - from end of text
}

function setPlayTime(time, ele) {
  var playTime = millisToMinutesAndSeconds(moment.duration(time));
  $(ele).find(".playtime").html(playTime);
}

function youtubeInfoAPI(ele) {
  var APIKEY = "AIzaSyBtApCbN_LZp_PlWG3e7YjZmfntE5gkHtk";
  $(ele).each(function () {
    var article = $(this);
    var video_url = $(article).find("a").attr("href");
    var video_id = video_url.split("?v=")[1];

    var xhttp = new XMLHttpRequest();
    xhttp.open(
      "GET",
      "https://www.googleapis.com/youtube/v3/videos?part=contentDetails&id=" +
      video_id +
      "&key=" +
      APIKEY,
      true
    );
    xhttp.send();
    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        var json = JSON.parse(this.responseText);
        var vTime = json.items[0]["contentDetails"]["duration"];
        setPlayTime(vTime, article);
      }
    };
  });
}

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}

// READ MORE
function readMore(jObj, lineNum, viewPort) {
  if (isNaN(lineNum)) {
    lineNum = 4;
  }

  var go = new ReadMore(jObj, lineNum, viewPort);
}

function newReadMore(ele, arrayOfObjects) {
  var eleWrapper = $(ele);
  // sort from smallest to largest
  var sortedSizes = arrayOfObjects.sort(function (a, b) {
    return a.viewportSize < b.viewportSize ? -1 : 1;
  });

  var readMoreLabelInactive = "Read More";
  var readMoreLabelActive = "Read Less";

  // get all the sizes from the object
  var sizes = [];
  sortedSizes.forEach(function (_ref) {
    var numberToShow = _ref.numberToShow,
      viewportSize = _ref.viewportSize;
    sizes.push(viewportSize);
  });

  // each ele so everything is relative to each item we call this on
  $(eleWrapper).each(function () {
    $(window).on("load resize", readMoreInit);
  });

  function readMoreInit() {
    var sizeToUse = sizes.filter(function (num) {
      return $(window).width() <= num;
    })[0];

    // console.log(typeof sortedSizes);
    // get the viewport size needed
    if (sizeToUse !== undefined) {
      var numberOfElementsToShow = sortedSizes.find(function (item) {
        return item.viewportSize == sizeToUse;
      });

      // console.log(numberOfElementsToShow);
    }

    // if out of size range and read more is active
    if (sizeToUse === undefined && $(eleWrapper).hasClass("active")) {
      deconstructReadMore();
    }

    // if inside of size range and read more is active
    if (sizeToUse !== undefined && $(eleWrapper).hasClass("active")) {
      // check to see if current size is same as before or if changed sizes
      if (!$(eleWrapper).hasClass(sizeToUse)) {
        deconstructReadMore();
        constructReadMore(
          eleWrapper,
          numberOfElementsToShow.numberToShow,
          sizeToUse
        );
      }
    }

    // if size is in range and read more isn't active
    if (sizeToUse !== undefined && !$(eleWrapper).hasClass("active")) {
      constructReadMore(
        eleWrapper,
        numberOfElementsToShow.numberToShow,
        sizeToUse
      );
    }
  }

  function constructReadMore(ele, num, className) {
    $(eleWrapper).addClass("active");
    $(eleWrapper).addClass("" + className);

    // get set of children
    var children = $(eleWrapper).children();

    // break them up into their own seperate parts
    var shownEles = children.slice(0, num);
    $(shownEles).wrapAll('<div class="shown-elements"></div>');

    // break them up into their own seperate parts
    var hiddenEles = children.slice(num);
    $(hiddenEles).wrapAll(
      '<div class="hidden-elements"><div class="hidden-elements-wrapper"></div></div>'
    );
    $(eleWrapper).find(".hidden-elements").css("display", "none");

    // wrap the rest with the container and add button
    $(eleWrapper).children().wrapAll('<div class="read-more-container"></div>');
    $(eleWrapper)
      .find(".read-more-container")
      .append('<a class="read-more btn" href="#">Read More</a>');

    $(eleWrapper)
      .find(".read-more")
      .click(function (e) {
        e.preventDefault();
        $(eleWrapper).find(".read-more-container").toggleClass("active");
        $(eleWrapper).find(".hidden-elements").slideToggle();
        $(this).toggleClass("active");

        if ($(this).hasClass("active")) {
          $(this).text(readMoreLabelActive);
        } else {
          $(this).text(readMoreLabelInactive);
        }
      });
  }

  function deconstructReadMore() {
    function Unwrapper(ele, target) {
      $(ele).find(target).children().unwrap();
    }
    Unwrapper(eleWrapper, ".hidden-elements-wrapper");
    Unwrapper(eleWrapper, ".hidden-elements");
    Unwrapper(eleWrapper, ".shown-elements");
    Unwrapper(eleWrapper, ".read-more-container");
    $(eleWrapper).find(".read-more").remove();
    sizes.map(function (item) {
      $(eleWrapper).removeClass("" + item);
    });
    $(eleWrapper).removeClass("active");
  }
}

var tag = document.createElement("script");
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName("script")[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  function createPlayers(eleId, videoId) {
    var playerWrapper = $("#" + eleId).parent();
    player = new YT.Player(eleId, {
      height: "390",
      width: "640",
      videoId: videoId,
      events: {
        onReady: onPlayerReady,
        onStateChange: onPlayerStateChange,
      },
    });
    $(playerWrapper).addClass("active");
  }

  $(".full-video-wrapper .yt-video-wrapper > div:not(.overlay)").each(
    function () {
      createPlayers($(this).attr("id"), $(this).attr("id"));
    }
  );

  function createPlayerChanger(eleId, videoId, videoTitle) {
    // console.log(eleId);
    var playerWrapper = $("#" + eleId).parent();

    player = new YT.Player(eleId, {
      height: "390",
      width: "640",
      videoId: videoId,
      events: {
        onReady: onPlayerReadyChanger,
        onStateChange: onPlayerStateChange,
      },
    });
    var theVideoId = player.getIframe();
    theVideoId.parentNode.parentNode.parentNode.querySelector(
      ".video-info .title"
    ).innerText = videoTitle;

    theVideoId.parentNode.parentNode.parentNode.querySelector(
      ".overlay"
    ).style.backgroundImage = "url(" + firstVideoImage + ")";

    $(playerWrapper).addClass("active");
  }

  var firstVideoId = $(".endorsments-sidebar .video")
    .first()
    .find("a")
    .addClass("active")
    .attr("data-youtube-id");
  var firstVideoImage = $(".endorsments-sidebar .video")
    .first()
    .find("a")
    .attr("data-youtube-image");
  var firstVideoTitle = $(".endorsments-sidebar .video")
    .first()
    .find(".video-title")
    .text();
  if ($("#featured-" + firstVideoId).length !== 0) {
    // console.log("dank");
    createPlayerChanger(
      "featured-" + firstVideoId,
      firstVideoId,
      firstVideoTitle
    );
  }
}

function onPlayerReadyChanger(event) {
  var wrapper = $(event.target.getIframe()).parent();
  var tabButtons = $(".tab-nav-item:not(.active)");

  function playYTVideo(wrapper, player) {
    $(wrapper).click(function () {
      $(this).addClass("playing");
      player.playVideo();
    });
  }
  function stopYTVideo(target, player) {
    $(target).click(function () {
      $(this).removeClass("playing");
      player.pauseVideo();
    });
  }
  stopYTVideo(tabButtons, event.target);
  playYTVideo(wrapper, event.target);

  $(".endorsments-sidebar .video a").click(function (e) {
    $(".endorsments-sidebar .video a").each(function () {
      $(this).removeClass("active");
    });
    $(this).addClass("active");
    $(wrapper).removeClass("playing");
    e.preventDefault();
    $(wrapper)
      .find(".overlay")
      .css(
        "background-image",
        "url(" + $(this).attr("data-youtube-image") + ")"
      );
    event.target.loadVideoById($(this).attr("data-youtube-id"));
    event.target.pauseVideo();

    var newTitle = $(this).find(".video-title").text();

    var theVideoId = document.querySelector("#" + event.target.getIframe());
    theVideoId.parentNode.parentNode.parentNode.querySelector(
      ".video-info .title"
    ).innerText = newTitle;
  });
}

function onPlayerReady(event) {
  var wrapper = $(event.target.getIframe()).parent();
  var tabButtons = $(".tab-nav-item:not(.active)");

  var videoData = event.target.getVideoData();
  var theVideoId = event.target.getIframe();
  theVideoId.parentNode.parentNode.parentNode.querySelector(
    ".video-info .title"
  ).innerText = videoData.title;

  function playYTVideo(wrapper, player) {
    $(wrapper).click(function () {
      $(this).addClass("playing");

      player.playVideo();
    });
  }

  function stopYTVideo(target, player) {
    $(target).click(function () {
      $(this).removeClass("playing");
      player.pauseVideo();
    });
  }

  stopYTVideo(tabButtons, event.target);
  playYTVideo(wrapper, event.target);
}

var done = false;
function onPlayerStateChange(event) {
  // if (event.data == YT.PlayerState.PLAYING && !done) {
  //   setTimeout(stopVideo, 6000);
  //   done = true;
  // }

  if (event.data == YT.PlayerState.BUFFERING) {
    // console.log("BUFFERING");
    var wrapper = $(event.target.getIframe()).parent();
    $(wrapper).addClass("playing");
    return;
  }

  if (
    event.data == YT.PlayerState.PAUSED ||
    event.data == YT.PlayerState.ENDED
  ) {
    // console.log("PAUSED");
    var wrapper = $(event.target.getIframe()).parent();
    player.pauseVideo();
    $(wrapper).removeClass("playing");
    return;
  }

  if (event.data == -1) {
    var wrapper = $(event.target.getIframe()).parent();
    player.pauseVideo();
    $(wrapper).removeClass("playing");
    return;
  }
}

function stopVideo() {
  player.stopVideo();
}

function checkoutAlert() {
  $("#wc_checkout_add_ons").after(
    '<p><strong>If you have opted to recieve a DVD copy, please <a href="https://theveilremoved.com/free-dvd-request/" style="color: #0074db !important;" target="_blank">click here to fill out the request form</a> before proceeding.</strong></p>'
  );
}

$(document.body).on('updated_cart_totals', function () {
  location.reload(true);
});